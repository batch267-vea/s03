<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03 - Classes and Objects</title>
</head>
<body>

	<h1>Objects from Variables</h1>
	<p><?php var_dump($buildingObj); ?></p>
	<!-- Object operator or single arrow notation is used to access or call methods and properties in PHP object -->
	<p><?php echo $buildingObj -> name; ?></p>

	<h1>Object from Classes</h1>
	<p><?php var_dump($building); ?></p>
	<!-- Accessing the method of the instantiated -->
	<p><?php echo $building-> printName(); ?></p>

	<h2>Modifying the Instantiated Object</h2>
	<?php $building->name = "GMA Network"; ?>
	<!-- Change the floor number, but provide a value of a string instead of int. -->
	<!-- this issue can be solved with encapsulation later on -->
	<?php $building->floors = "Twenty"; ?>
	<p><?php var_dump($building); ?></p>
	<!-- Accessing the method of the instantiated -->
	<p><?php echo $building-> printName(); ?></p>


	<h1>Inheritance (Condiminium Object)</h1>
	<p><?php var_dump($condominium) ?></p>
	<p><?= $condominium->name; ?></p>
	<p><?= $condominium->floors; ?></p>
	<p><?= $condominium->address; ?></p>

	<h1>Polymorphism (Overriding the behavior of the printName() method)</h1>
	<p><?php echo $condominium->printName(); ?></p>


</body>
</html>